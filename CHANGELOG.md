# CHANGELOG

## Version : 0.22.0

- fix: shortcut : See merge request shun-fix9-labo/config-files!50


## Version : 0.21.0

- fix: shortcut : See merge request shun-fix9-labo/config-files!49


## Version : 0.20.0

- fix: gitlab-ci : See merge request shun-fix9-labo/config-files!48
- fix: shortcut command : See merge request shun-fix9-labo/config-files!47

# Version : 0.19.0

fix: labo-container env path

# Version : 0.18.0

fix: load container env

# Version : 0.17.0

fix: shortcut

# Version : 0.16.0

fix: coc resource path

# Version : 0.15.0

fix: config

# Version : 0.14.0

fix: dein plugins

# Version : 0.13.0

fix: vimrc

# Version : 0.12.0

fix: branch count

# Version : 0.11.0

fix: prompt

# Version : 0.10.0

fix: config

# Version : 0.9.0

fix: config

# Version : 0.8.0

fix: remove all swapfile when fish initialized

# Version : 0.7.0

fix: config

# Version : 0.6.0

fix: config

# Version : 0.5.0

fix: set swapfile path

# Version : 0.4.0

fix: vim config

# Version : 0.3.0

fix: vi alias

# Version : 0.2.0

fix: LESS charset

# Version : 0.1.2

fix: gitlab-ci

# Version : 0.1.1

fix: gitlab-ci

# Version : 0.1.0

add: gitlab-ci

